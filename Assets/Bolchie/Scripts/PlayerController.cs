﻿using UnityEngine;
using System.Linq;

//Basic Player Script//
//controls: 
//A, D, Left, Right to move
//Left Alt to attack
//Space to jump
//Z is to see dead animation

public class PlayerController : MonoBehaviour
{
    public bool _warpEnabled = false;
    public PhotonView photonView;
    //variable for how fast player runs//
    private float speed = 5f;

    private bool facingRight = true;
    private Animator anim;
    bool grounded = false;
    public Transform groundCheck;
    float groundRadius = 0.2f;
    public LayerMask whatIsGround;
    public bool InArena = false;

    public Vector3 _positionPlayer1BeforeArena;
    public Vector3 _positionPlayer2BeforeArena;
    //variable for how high player jumps//
    [SerializeField]
    private float jumpForce = 300f;

    public Rigidbody2D rb { get; set; }

    public int playerID = 0;

    bool dead = false;
    public bool attack = false;

    private readonly byte _movePlayerToArenaEventCode = 0;
    private readonly byte _movePlayerToLaneEventCode = 1;

    public Collider2D AttackTrigger;

    private GameObject PhotonManager;

    //Important so much
    Vector3[] positions = new Vector3[2];

    void Start()
    {
        GetComponent<Rigidbody2D>().freezeRotation = true;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
        PhotonNetwork.OnEventCall += OnEvent;
        object[] parameters = new object[] { playerID };
        photonView.RPC("changeSprite", PhotonTargets.AllBuffered, parameters);
    }

    void Update()
    {
        if (photonView.isMine)
        {
            HandleInput();
        }
    }

    [PunRPC]
    void changeSprite(int id)
    {
        Color color = id == 1 ? Color.red : Color.blue;
        GetComponent<SpriteRenderer>().color = color;
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(transform.localScale);
            stream.SendNext(playerID);
        }
        else
        {
            transform.localScale = (Vector3)stream.ReceiveNext();
            playerID = (int)stream.ReceiveNext();          
        }
    }

    //movement//
    void FixedUpdate()
    {
        if (photonView.isMine)
        {
            grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
            anim.SetBool("Ground", grounded);

            float horizontal = Input.GetAxis("Horizontal");
            if (!dead && !attack)
            {
                anim.SetFloat("vSpeed", rb.velocity.y);
                anim.SetFloat("Speed", Mathf.Abs(horizontal));
                rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
            }
            if (horizontal > 0 && !facingRight && !dead && !attack)
            {
                Flip(horizontal);
            }

            else if (horizontal < 0 && facingRight && !dead && !attack)
            {
                Flip(horizontal);
            }
        }
    }

    //attacking and jumping//
    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftAlt) && !dead)
        {
            attack = true;
            anim.SetBool("Attack", true);
            anim.SetFloat("Speed", 0);
            AttackTrigger.enabled = true;
            GameObject.Find("hit").GetComponent<AudioSource>().Play();
        }
        if (Input.GetKeyUp(KeyCode.LeftAlt))
        {
            attack = false;
            anim.SetBool("Attack", false);
            AttackTrigger.enabled = false;
        }
        if (grounded && Input.GetKeyDown(KeyCode.Space) && !dead)
        {
            anim.SetBool("Ground", false);
            rb.AddForce(new Vector2(0, jumpForce));
            GameObject.Find("jump").GetComponent<AudioSource>().Play();
        }

        //warp//
        if (_warpEnabled == true)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                photonView.RPC("setBoolInArena", PhotonTargets.AllBuffered, playerID, this.transform.position);
                SpawnPlayerInArenaEventRaise();

                Debug.Log("pressed E in Warp");
            }
        }
    }

    //Sollte man weglöschen not sure yet
    public void Die()
    {
        anim.SetBool("Dead", true);
        anim.SetFloat("Speed", 0);
        dead = true;
    }

    private void SpawnPlayerInArenaEventRaise()
    {
        byte evCode = 0;
        object[] content = new object[] { };
        bool reliable = true;
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(evCode, content, reliable, raiseEventOptions);
    }

    private void SpawnInArena()
    {
        GameObject.Find("swoosh").GetComponent<AudioSource>().Play();
        GameObject.Find("BackgroundMusic").GetComponent<AudioSource>().Stop();
        GameObject.Find("arenaSong").GetComponent<AudioSource>().Play();
        Vector3 _warpSpawnPos = GameObject.Find("ArenaSpawn" + playerID).transform.position;
        this.transform.position = _warpSpawnPos;
        this.gameObject.GetComponent<PlayerCameraController>().SetToArenaCamera();
    }

    public void SpawnPlayerInLaneEventRaise()
    {
        byte evCode = 1;
        object[] content = new object[] { };
        bool reliable = true;
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(evCode, content, reliable, raiseEventOptions);
    }

    private void SpawnInLane()
    {
        var camController = this.gameObject.GetComponent<PlayerCameraController>();
        camController.boundaries = false;

        var arenaManager = GameObject.Find("ArenaManager");
        var first = arenaManager.GetComponent<ArenaLogic>().FirstPlayerPos;
        var second = arenaManager.GetComponent<ArenaLogic>().SecondPlayerPos;

        Vector3 front = first.x > second.x ? first : second;
        Vector3 back = first.x > second.x ? second : first;

        if (playerID == 1)
        {
            if (this.GetComponent<PlayerLife>().DiedInArena)
            {
                this.transform.position = new Vector3(back.x, -3, -1);
            }
            else
            {
                this.transform.position = new Vector3(front.x, -3, -1);
            }
        }

        if (playerID == 2)
        {
            if (this.GetComponent<PlayerLife>().DiedInArena)
            {
                this.transform.position = new Vector3(back.x, 5, -1);
            }
            else
            {
                this.transform.position = new Vector3(front.x, 5, -1);
            }
        }

        this.gameObject.GetComponent<PlayerLife>().DiedInArena = false;
        this.gameObject.GetComponent<PlayerCameraController>().SetToPlayerCamera();
        camController.boundaries = true;
        InArena = false;


        GameObject.Find("swoosh").GetComponent<AudioSource>().Play();
        GameObject.Find("BackgroundMusic").GetComponent<AudioSource>().Play();
        GameObject.Find("arenaSong").GetComponent<AudioSource>().Stop();
    }

    public void OnEvent(byte eventCode, object content, int senderId)
    {
        if (eventCode == _movePlayerToArenaEventCode)
        {
            SpawnInArena();
        }
        else if (eventCode == _movePlayerToLaneEventCode)
        {
            GetComponent<PlayerLife>().HitPoints = 150;
            GetComponent<PlayerLife>().UpdateLifeBar();
            SpawnInLane();
        }
        else
        {
            Debug.Log("Something went wrong in OnEvent in PlayerController.cs");
        }
    }

    [PunRPC]
    private void setBoolInArena(int id, Vector3 pos)
    {
        var players = GameObject.FindGameObjectsWithTag("Player");      
        foreach (GameObject p in players)
        {
            p.GetComponent<PlayerController>().InArena = true;
        }
    }

    private void Flip(float horizontal)
    {
        if (photonView.isMine)
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }
}
