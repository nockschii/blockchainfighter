﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileWarp : MonoBehaviour
{

    [SerializeField]
    private bool _isUsed;

    public ParticleSystem WarpParticles;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_isUsed == false)
        {
            if (collision.tag == "Player")
            {
                collision.GetComponent<PlayerController>()._warpEnabled = true;
                _isUsed = true;
                this.GetComponentInChildren<ParticleSystem>().Stop();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<PlayerController>()._warpEnabled = false;
        }
    }
}
