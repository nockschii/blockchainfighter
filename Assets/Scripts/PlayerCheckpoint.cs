﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCheckpoint : MonoBehaviour {

    public Vector3 _savedPosition;
    
    private void Start()
    {
        // start position
        _savedPosition = transform.position;
        Debug.Log(_savedPosition);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Checkpoint")
        {
            // new position
            _savedPosition = collision.gameObject.transform.position;
            Debug.Log(_savedPosition);
        }
    }
}
