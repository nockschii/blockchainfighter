﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileHeal : MonoBehaviour {

    private bool _isUsed;

    [SerializeField]
    private float _healPoints = 10;

    private void Start()
    {
        _isUsed = false;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!_isUsed)
        {
            Debug.Log(collision.tag);
            PlayerLife lb = (PlayerLife)collision.gameObject.GetComponent("PlayerLife");

            lb.HealDamage(_healPoints);
            _isUsed = true;
        }
        Debug.Log("Already used");
    }
}
