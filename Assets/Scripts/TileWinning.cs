﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TileWinning : MonoBehaviour {

    public GameObject gameOver;
    public Text playerHasWon;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            GameObject.Find("BackgroundMusic").GetComponent<AudioSource>().Stop();
            GameObject.Find("winnerSong").GetComponent<AudioSource>().Play();
            var winner = collision.GetComponent<PlayerController>().playerID;
            gameOver.SetActive(true);
            playerHasWon.text = "Player " + winner + " has won.";
        }
    }
}
