﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerCameraController : MonoBehaviour
{
    private List<Camera> cameras;
    private Vector3 _offset;
    private int _playerID;
    public Vector3 MaxPlayerPos;

    private Vector3 _minCameraPos = new Vector3(-50, 0, 0);
    public Vector3 MaxCameraPos = new Vector3(1000, 0, 0);

    public bool boundaries = true;

    GameObject healthbar1;
    Vector3 oldPos1;
    GameObject healthbar2;
    Vector3 oldPos2;

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        info.sender.TagObject = this.gameObject;
    }

    void Start()
    {
        _playerID = this.GetComponent<PlayerController>().playerID;

        cameras = Camera.allCameras.ToList<Camera>();
        var _camera = cameras.Where(c => c.name == "CameraPlayer" + _playerID).First();

        _offset = _camera.transform.position - this.transform.position;

        var otherCams = cameras.Where(c => c.name != "CameraPlayer" + _playerID);
        foreach (var item in otherCams)
        {
            item.enabled = false;
        }
        MaxPlayerPos = transform.position;
        MaxCameraPos = _camera.transform.position;

        healthbar1 = GameObject.Find("FirstPlayerHealthBarBackground");
        oldPos1 = healthbar1.transform.position;
        healthbar2 = GameObject.Find("SecondPlayerHealthBarBackground");
        oldPos2 = healthbar2.transform.position;
    }

    void FixedUpdate()
    {
        if (transform.position.x > MaxPlayerPos.x)
        {
            MaxPlayerPos = transform.position;
        }
        else
        {
            if (boundaries)
            {
                transform.position = new Vector3(
                Mathf.Clamp(transform.position.x, MaxPlayerPos.x - 8, MaxPlayerPos.x + 100),
                transform.position.y,
                transform.position.z
                );
            }
            //else
            //{
            //    transform.position = new Vector3(
            //    Mathf.Clamp(transform.position.x, -100, 1000),
            //    transform.position.y,
            //    transform.position.z
            //    );
            //}
        }



        var _camera = cameras.Where(c => c.name == "CameraPlayer" + _playerID).First();
        if (_camera.transform.position.x < (this.transform.position + _offset).x)
        {
            _camera.transform.position = new Vector3
            (
            Mathf.Clamp((this.transform.position + _offset).x, MaxCameraPos.x, MaxCameraPos.x + 10000),
            Mathf.Clamp(_camera.transform.position.y, _minCameraPos.y, MaxCameraPos.y),
            _camera.transform.position.z
            );
        }

        if (_camera.transform.position.x > MaxCameraPos.x)
        {
            MaxCameraPos = _camera.transform.position;
        }
    }

    public void ResetCameraToPos(Vector3 pos)
    {
        Vector3 tmpPos = pos + _offset;
        var _camera = cameras.Where(c => c.name == "CameraPlayer" + _playerID).First();  
        _camera.transform.position = new Vector3(tmpPos.x , 0, -10); //-10 wegen der Ebene
    }

    public void SetToArenaCamera()
    {
        var _cameraArena = cameras.Where(c => c.name == "CameraArena").First();
        boundaries = false;
        _cameraArena.enabled = true;

        var otherCams = cameras.Where(c => c.name != "CameraArena");
        foreach (var item in otherCams)
        {
            item.enabled = false;
        }
        SetHUDposToArena();
    }

    public void SetToPlayerCamera()
    {
        var _camera = cameras.Where(c => c.name == "CameraPlayer" + _playerID).First();

        var otherCams = cameras.Where(c => c.name != _camera.name);
        foreach (var item in otherCams)
        {
            item.enabled = false;
        }

        _camera.enabled = true;
        SetHUDposToLane();

        boundaries = true;
    }

    public void SetHUDposToArena()
    {
        healthbar1.transform.position = GUIUtility.ScreenToGUIPoint(new Vector3(250, 900, 0));
        healthbar2.transform.position = GUIUtility.ScreenToGUIPoint(new Vector3(1400, 900, 0));
    }

    public void SetHUDposToLane()
    {
        healthbar1.transform.position = oldPos1;
        healthbar2.transform.position = oldPos2;
    }
}
