﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class PlayerLife : MonoBehaviour {

    private Image _currentLifeBar;
    private PlayerCheckpoint _playerCheckpoint;
    private PlayerController _playerController;
    public float HitPoints = 150;
    private float _maxHitPoint = 150;
    public bool DiedInArena = false;
    private int _myPlayerId = 0;
    private List<Image> _lifeBars;

    // Use this for initialization
    void Start()
    {
        // player
        _playerController = GetComponent<PlayerController>();
        _playerCheckpoint = GetComponent<PlayerCheckpoint>();
        _myPlayerId = this.gameObject.GetComponent<PlayerController>().playerID;
        Debug.Log("PID CHANGED LUL WTF?!"+ _myPlayerId);
        // bars
        _lifeBars = new List<Image>(2);
        _lifeBars.Add(GameObject.FindWithTag("PlayerOneBar").GetComponent<Image>());
        _lifeBars.Add(GameObject.FindWithTag("PlayerTwoBar").GetComponent<Image>());
        _currentLifeBar = _lifeBars[_myPlayerId - 1];

        UpdateLifeBar();
	}

    public void UpdateLifeBar()
    {
        float ratio = HitPoints / _maxHitPoint;
        _currentLifeBar.fillAmount = ratio;
    }

    public void TakeDamage(float damage)
    {
        int playerId = _playerController.playerID;
        HitPoints -= damage;
        if (HitPoints <= 0)
        {
            if (_playerController.InArena)
            {
                Debug.Log("Died in arena");
                DiedInArena = true;
                _playerController.SpawnPlayerInLaneEventRaise();
            }
            else if (!_playerController.InArena)
            {
                Debug.Log("Died in lane");
                HitPoints = _maxHitPoint;
                RespawnPlayer();
            }
            //transform.GetComponent<PhotonView>().RPC("ChangeHealth", PhotonTargets.AllBuffered, damage, playerId);
        }
        UpdateLifeBar();
    }
    //[PunRPC]
    private void ChangeHealth(float damage, int playerId)
    {
        Debug.Log("PunRPC DMG: " + damage);
        Debug.Log("PunRPC PID: " + playerId);
        Debug.Log("My Own PID: " + _myPlayerId);
        if (playerId == _myPlayerId)
        {
            HitPoints = HitPoints - damage;
            if (HitPoints <= 0)
            {
                //HitPoints = 0;
                if (_playerController.InArena)
                {
                    Debug.Log("Died in arena");
                    DiedInArena = true;
                    _playerController.SpawnPlayerInLaneEventRaise();
                }
                else if (!_playerController.InArena)
                {
                    Debug.Log("Died in lane");
                    HitPoints = _maxHitPoint;
                    RespawnPlayer();
                }
                else
                {
                    Debug.Log("Something went wrong at TakeDamage in PlayerLife.cs");
                }
            }
        }

        foreach (var bar in _lifeBars)
        {
            bar.fillAmount = HitPoints / _maxHitPoint;
        }
    }

    private void RespawnPlayer()
    {
        var camController = this.gameObject.GetComponent<PlayerCameraController>();
        camController.boundaries = false;
        
        this.transform.position = _playerCheckpoint._savedPosition;
        camController.MaxPlayerPos.x = _playerCheckpoint._savedPosition.x;
        camController.MaxCameraPos.x = _playerCheckpoint._savedPosition.x;
        camController.ResetCameraToPos(_playerCheckpoint._savedPosition);

        camController.boundaries = true;
    }


    public void HealDamage(float heal)
    {
        HitPoints = HitPoints + heal;
        if (HitPoints > _maxHitPoint)
        {
            HitPoints = _maxHitPoint;
        }
        UpdateLifeBar();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "AttackBox")
        {
            this.TakeDamage(30);
            Debug.Log("lost health");
        }
    }
}
