﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class photonConnect : MonoBehaviour
{
    //stfu ali
    public string Version = "0.1";
    public InputField JoinServerName;
    public InputField HostServerName;
    public GameObject ConnectedString;
    public GameObject DisconnectedString;
    public GameObject JoinServer;
    public GameObject HostServer;
    public GameObject ConnectMasterServer;

    public void Awake()
    {
        PhotonNetwork.ConnectUsingSettings(Version);
        Debug.Log("Connecting to master..");
    }

    public void onClickCreateRoom()
    {
        if (HostServerName.text != null)
        {
            PhotonNetwork.CreateRoom(HostServerName.text, new RoomOptions() {MaxPlayers = 2}, null);
        }
        else
        {
            Debug.LogError("Roomname required");
            //EditorUtility.DisplayDialog("Servername empty", "Please enter servername", "Ok");
        }
    }

    public void onClickJoinRoom()
    {
        if (JoinServerName.text != null)
        {
            PhotonNetwork.JoinRoom(JoinServerName.text);
        }
        else
        {
            Debug.LogError("Roomname required");
        }
    }

    private void OnJoinedRoom()
    {
        Debug.Log("Joined room");
        PhotonNetwork.LoadLevel("Game");
    }

    private void OnConnectedToMaster()
    {
        Debug.Log("OnConnected to master");
        SwitchConnectedStatus();
    }

    private void OnDisconnectedFromPhoton()
    {
        Debug.Log("OnDISConnected to master");
        SwitchConnectedStatus();
    }

    private void SwitchConnectedStatus()
    {
        ConnectedString.active = PhotonNetwork.connected;
        DisconnectedString.active = !PhotonNetwork.connected;
        ConnectMasterServer.active = !PhotonNetwork.connected;
        JoinServer.active = HostServer.active = PhotonNetwork.connected;
    }

    // Update is called once per frame
    void Update ()
    {
        Debug.Log("Connected players:"+PhotonNetwork.playerList.Length);
    }
}
