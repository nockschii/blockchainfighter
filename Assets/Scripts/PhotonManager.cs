﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PhotonManager : MonoBehaviour
{

    public string Version = "0.1";
    public InputField JoinServerName;
    public InputField HostServerName;
    public GameObject ConnectedString;
    public GameObject DisconnectedString;
    public GameObject JoinServer;
    public GameObject HostServer;
    public GameObject ConnectMasterServer;

    public List<GameObject> Players;
    private Vector3 _player1Pos = new Vector3(-8.5f, -6.5f, -1);
    private Vector3 _player2Pos = new Vector3(-8.5f, 3.5f, -1);

    public void Awake()
    {
        Players = new List<GameObject>(2);
        PhotonNetwork.ConnectUsingSettings(Version);
        Debug.Log("Connecting to master..");
        var m = Resources.FindObjectsOfTypeAll(typeof(PhotonManager));
        Debug.Log(m + "pm1");
        if (m.Length < 2)
        {
            Debug.Log(m + "pm2");
            DontDestroyOnLoad(this);
        }
        SceneManager.sceneLoaded += OnSceneFinishedLoading;
    }

    public void onClickCreateRoom()
    {
        if (HostServerName.text != null)
        {
            PhotonNetwork.CreateRoom(HostServerName.text, new RoomOptions() {MaxPlayers = 2}, null);
        }
        else
        {
            Debug.LogError("Roomname required");
            //EditorUtility.DisplayDialog("Servername empty", "Please enter servername", "Ok");
        }
    }

    public void onClickJoinRoom()
    {
        if (JoinServerName.text != null)
        {
            PhotonNetwork.JoinRoom(JoinServerName.text);
        }
        else
        {
            Debug.LogError("Roomname required");
        }
    }

    private void OnJoinedRoom()
    {
        Debug.Log("Joined room");
        PhotonNetwork.LoadLevel("Game");
    }

    private void OnConnectedToMaster()
    {
        Debug.Log("OnConnected to master");
        SwitchConnectedStatus();
    }

    private void OnDisconnectedFromPhoton()
    {
        Debug.Log("OnDISConnected to master");
        SwitchConnectedStatus();
    }

    private void SwitchConnectedStatus()
    {
        ConnectedString.active = PhotonNetwork.connected;
        DisconnectedString.active = !PhotonNetwork.connected;
        ConnectMasterServer.active = !PhotonNetwork.connected;
        JoinServer.active = HostServer.active = PhotonNetwork.connected;
    }

    private void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Loaded " + scene.name);
        if (scene.name == "Game")
        {
            spawnPlayer();
        }
    }

    private void spawnPlayer()
    {
        if (PhotonNetwork.playerList.Length == 1) //it's player 1
        {
            GameObject player = PhotonNetwork.Instantiate("Player", _player1Pos, Quaternion.identity, 0);
            player.GetComponent<PlayerController>().playerID = 1;
            Players.Add(player);
        }
        else //it's player 2
        {
            GameObject player = PhotonNetwork.Instantiate("Player", _player2Pos, Quaternion.identity, 0);
            player.GetComponent<PlayerController>().playerID = 2;
            Players.Add(player);
        }
        Debug.Log(Players.Count + "players count");
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.DestroyAll();
        Debug.Log("OnLeftRoom");
        PhotonNetwork.LoadLevel("MainMenu");
    }
}
