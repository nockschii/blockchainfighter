﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaLogic : MonoBehaviour {

    public Vector3 FirstPlayerPos;
    public Vector3 SecondPlayerPos;

    private void Start()
    {

    }

    private void Update()
    {
        var players = GameObject.FindGameObjectsWithTag("Player");

        if(!players[0].GetComponent<PlayerController>().InArena)
        {
            FirstPlayerPos = players[0].transform.position;
            SecondPlayerPos = players[1].transform.position;
        }       
    }
}
