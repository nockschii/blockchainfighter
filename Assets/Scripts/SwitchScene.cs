﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour {
    public GameObject PlayerClone;
    public PhotonManager pm;

    public void BackToMainMenu()
    {
        
        PlayerClone = GameObject.FindGameObjectWithTag("Player");
        pm = GameObject.Find("PhotonManager").GetComponent<PhotonManager>();
        PlayerClone = new GameObject();
        pm.LeaveRoom();
    }
}
