﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileHurt : MonoBehaviour {

    [SerializeField]
    private float _damagePoints = 50;

    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Entering OnTriggerEnter2D TileHuRt");
               
        Debug.Log(collision.tag);
        PlayerLife PlayerLife = (PlayerLife) collision.gameObject.GetComponent("PlayerLife");

        PlayerLife.TakeDamage(_damagePoints);

        if (_damagePoints == 50)
        {
            GameObject.Find("dmgUh").GetComponent<AudioSource>().Play();
        }
        else if (_damagePoints == 150)
        {
            GameObject.Find("dmgAh").GetComponent<AudioSource>().Play();
        }
    }
}
