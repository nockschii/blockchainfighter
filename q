*   [33m9d28ee5[m[33m ([m[1;36mHEAD -> [m[1;32mdev[m[33m, [m[1;31morigin/dev[m[33m)[m Merge branch 'HealthBar' into dev
[31m|[m[32m\[m  
[31m|[m * [33m363cf1e[m fixed something with LifeTrigger script
[31m|[m * [33med0adc8[m added LifeBar and LifeTrigger Script
* [32m|[m [33mf54afa8[m implemented camera movement and boundaries
[32m|[m[32m/[m  
*   [33m9d91267[m Merge branch 'photonTesting' into 'dev'
[33m|[m[34m\[m  
* [34m\[m   [33m8afba13[m Merge branch 'photonTesting' into 'dev'
[35m|[m[36m\[m [34m\[m  
[35m|[m [36m|[m [34m|[m * [33mfc262db[m[33m ([m[1;31morigin/photonTesting[m[33m)[m fixed jumping synchronisation (Rigidbody2D)
[35m|[m [36m|[m [34m|[m * [33m20e558a[m Player2 now spawns on his lane
[35m|[m [36m|[m [34m|[m * [33mc637c56[m Fixed Flip synchronisation on photon
[35m|[m [36m|[m [34m|[m * [33mdcabdb3[m Fixed some synchronisation problems in photon
[35m|[m [36m|[m [34m|[m[34m/[m  
[35m|[m [36m|[m * [33md7b12a6[m added 2nd map
[35m|[m [36m|[m[36m/[m  
[35m|[m * [33m72a75dc[m initial commit for working
[35m|[m * [33maaa079d[m added player and a basic map
[35m|[m * [33md289b6d[m added photon asset and basic implementation
[35m|[m[35m/[m  
* [33mb3aabc1[m[33m ([m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m, [m[1;32mmaster[m[33m)[m initial commit
* [33mb2df63e[m added gitignore
* [33m4ad6532[m Initial commit
