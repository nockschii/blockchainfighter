# Blockchainfighter 3000
[GITLAB DOWNLOAD HERE](https://gitlab.com/nockschii/blockchainfighter)

![Titlepic](titlepic.jpg)

BCF is a platformer, where the goal is to reach the end of the map faster than your opponent.
On the way to the end, you have "warp-stones" where u can warp yourself and your opponent to an arena. In the arena you have to fight your enenmy in order to get closer to the goal.

## Group Members
- David Walser
- Ralph Quidet
- Ali Uz

## Controls
- movement: W, A, S, D
- jump: SPACE
- attack: ALT
- warp: E

## Gameplay Features
- 2D map
- sound effects
- fighting
- beautiful (selfmade) background music
- basic platformer movement

## Technical Features (Unity and other)
- networking over Photon
- particle effects
- audio mixer with different groups
- ingame gui with lifebars
- main menu
- materials are on fleek

## Time Spent report
- The most challenging part of this project were the network components. This took us long, but this was anticipated. So networking took us 50% of the projekt time.
- Debuging online multiplayer games is a pain in the ass :-(
- The gameplay was rather easy to implement, this was also expected.
- 42 hours as the time budget is not THAT much :-(
- The following charts show the amount of time spent in this project:
![timesheet](timesheet.jpg "timesheet")

## Problems and Challenges
Too many to solve ;-)

## Resources, Sources, References and Links
- [Photon Unity Networking Classic](https://assetstore.unity.com/packages/tools/network/photon-unity-networking-classic-free-1786)
- [Bolchie Unity with animation and stuff](https://assetstore.unity.com/packages/2d/characters/2d-character-bolchie-114179)
- [TILESET: Too Cube Forest, the free 2D platformer game tile set.](https://assetstore.unity.com/packages/2d/environments/too-cube-forest-the-free-2d-platformer-game-tile-set-117493)


## Self Assesment
- Game Design: 35
- Technical/Unity Features: 20
- meta: 15
---------
- total: 80